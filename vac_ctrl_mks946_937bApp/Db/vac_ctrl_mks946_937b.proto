################################################################################
# Protocol file for MKS946 Vacuum Mass Flow & Gauge Controller
# Protocol file for MKS937B Gauge Controller
# Base unit parameters
#
################################################################################

Terminator  = ";FF";

ReadTimeout   = 5000;
WriteTimeout  = 5000;
PollPeriod    = 100;
ReplyTimeout  = 5000;
LockTimeout   = 30000;

ExtraInput    = Ignore;

start_comm    = "@254";
ack_resp      = "@%*dACK";
nak_resp      = "@%*dNAK";
start_comm    = "@%(\$1)03d";
ack_resp      = "@%(\$1)=03dACK";
nak_resp      = "@%(\$1)=03dNAK";

on_off        = "%{OFF|ON}";
dis_enable    = "%{DISABLE|ENABLE}";

reconnect {
    disconnect;
    wait 1000;
    connect 1000;
}
@replytimeout {
    reconnect;
}

################################################################################
################################################################################
#
# System
#
################################################################################
################################################################################

################################################################################
# Get serial address of connected device
#  NOTE:
#   The device needs to be the only one connected to this serial connection
################################################################################
get_address
{
    out  "@254AD?";
    in   "@%*dACK%d";

    @init {
        out  "@254AD?";
        in   "@%*dACK%d";
    }
}

################################################################################
# Get baudrate of serial communication
# Protocol arguments:
#  1. Serial address
################################################################################
get_baudrate {
    out  $start_comm "BR?";
    in   $ack_resp   "%d";
}

################################################################################
# Set baudrate of serial communication
# Protocol arguments:
#  1. Serial address
################################################################################
baudrate="%d";
set_baudrate {
    out  $start_comm "BR!" $baudrate;
    in   $ack_resp         $baudrate;
}

################################################################################
# Get parity of serial communication
# Protocol arguments:
#  1. Serial address
################################################################################
get_parity {
    out  $start_comm "PAR?";
    in   $ack_resp   "%{NONE|EVEN|ODD}";
}

################################################################################
# Get communication delay of RS485 communication
# Protocol arguments:
#  1. Serial address
################################################################################
get_delay {
    out  $start_comm "DLY?";
    in   $ack_resp   "%d";
}

################################################################################
# Get unit of pressure measurement
# Protocol arguments:
#  1. Serial address
################################################################################
units = "%#{TORR|mBAR|MBAR=1|PASCAL|MICRON}";
get_unit {
    out  $start_comm "U?";
    in   $ack_resp   $units;
}

################################################################################
# Set unit of pressure measurement
# Protocol arguments:
#  1. Serial address
################################################################################
set_unit {
    out  $start_comm "U!" $units;
    in   $ack_resp        $units;
}

################################################################################
# Get display mode of device
# Protocol arguments:
#  1. Serial address
################################################################################
display_modes = "%{STD|LRG}";
get_display_mode {
    out  $start_comm "DM?";
    in   $ack_resp   $display_modes;
}

################################################################################
# Set display mode of device
# Protocol arguments:
#  1. Serial address
################################################################################
set_display_mode {
    out  $start_comm "DM!" $display_modes;
    in   $ack_resp         $display_modes;
}

################################################################################
# Get display format of device
# Protocol arguments:
#  1. Serial address
################################################################################
display_formats = "%{DEFAULT|PATCHZ|HIGHR}";
get_display_format {
    out  $start_comm "DF?";
    in   $ack_resp   $display_formats;
}

################################################################################
# Set display format of device
# Protocol arguments:
#  1. Serial address
################################################################################
set_display_format {
    out  $start_comm "DF!" $display_formats;
    in   $ack_resp         $display_formats;
}

################################################################################
# Check if front panel lock is enabled
# Protocol arguments:
#  1. Serial address
################################################################################
get_fp_lock {
    out  $start_comm "LOCK?";
    in   $ack_resp   $on_off;
}

################################################################################
# Enable or disable front panel lock
# Protocol arguments:
#  1. Serial address
################################################################################
set_fp_lock {
    out  $start_comm "LOCK!" $on_off;
    in   $ack_resp           $on_off;
}

################################################################################
# Check if user calibration is enabled
# Protocol arguments:
#  1. Serial address
################################################################################
get_calib {
    out  $start_comm "CAL?";
    in   $ack_resp   $dis_enable;
}

################################################################################
# Enable or disable user calibration
# Protocol arguments:
#  1. Serial address
################################################################################
set_calib {
    out  $start_comm "CAL!" $dis_enable;
    in   $ack_resp          $dis_enable;
}

################################################################################
# Check if parameter setting is enabled
# Protocol arguments:
#  1. Serial address
################################################################################
get_param_setting {
    out  $start_comm "SPM?";
    in   $ack_resp   $dis_enable;
}

################################################################################
# Enable or disable parameter setting
# Protocol arguments:
#  1. Serial address
################################################################################
set_param_setting {
    out  $start_comm "SPM!" $dis_enable;
    in   $ack_resp          $dis_enable;
}

################################################################################
# Retrieves the sensor module types
# Protocol arguments:
#  1. Serial address
################################################################################
module_types = "{NC|CC|HC|CM|PR|FC}";
get_module_types {
    out  $start_comm "MT?";
    in   $ack_resp   "%(A)" $module_types ","
                     "%(B)" $module_types ","
                     "%(C)" $module_types ","
                     "%(D){NA|PB|PC}";
}

################################################################################
# Retrieves the connected sensor type on the specified module
# Protocol arguments:
#  1. Serial address
#  2. The module; A, B, or C
################################################################################
get_sensor_types {
    out  $start_comm "ST\$2?";
    in   $ack_resp   "%(AA)[^,]"
                     "%(BB).1/,?(.*)/";
}

################################################################################
# Retrieves the type of the controller
# Protocol arguments:
#  1. Serial address
################################################################################
get_ctrl_type {
    out  $start_comm "MD?";
    in   $ack_resp   "%s";
}

################################################################################
# Retrieves the firmware version of the module
# Protocol arguments:
#  1. Serial address
#  2. The module;
#     1 = Slot A
#     2 = Slot B
#     3 = Slot C
#     4 = AIO
#     5 = COMM
#     6 = Main
################################################################################
get_fw_version {
    out  $start_comm "FV\$2?";
    in   $ack_resp   "%?f";
}

################################################################################
# Retrieves the serial number of the unit
# Protocol arguments:
#  1. Serial address
################################################################################
get_serial_number {
    out  $start_comm "SN?";
    in   $ack_resp   "%s";
}

################################################################################
# Retrieves the serial number of a module
# Protocol arguments:
#  1. Serial address
#  2. The module:
#     1 = Slot A
#     2 = Slot B
#     3 = Slot C
#     4 = AIO
#     5 = COMM
################################################################################
get_mod_serial_number {
    out  $start_comm "SN\$2?";
    in   $ack_resp   "%s";
}

################################################################################
# Retrieves the combination channel setting for the specified channel
# The format of the result is the plain string as returned by the device:
#  HP,MP,LP
# Protocol arguments:
#  1. Serial address
#  2. The combination channel number; 1 or 2
################################################################################
get_combination_channel_setting_str {
    out  $start_comm "SPC\$2?";
    in   $ack_resp   "%s";
}

################################################################################
# Retrieves the combination channel setting for the specified channel
# Protocol arguments:
#  1. Serial address
#  2. The combination channel number; 1 or 2
################################################################################
comb_channels = "{NA|A1|A2|B1|B2|C1|C2}";
get_combination_channel_setting {
    out  $start_comm "SPC\$2?";
    in   $ack_resp   "%(A)" $comb_channels ","
                     "%(B)" $comb_channels ","
                     "%(C)" $comb_channels;
}

################################################################################
# Sets the combination channel setting for the specified channel
# The format of the input is the plain string as expected by the device:
#  HP,MP,LP
# Protocol arguments:
#  1. Serial address
#  2. The combination channel number; 1 or 2
################################################################################
set_combination_channel_setting_str {
    out  $start_comm "SPC\$2!%s";
    in   $ack_resp   "%s";
}

################################################################################
# Check if the combination channel is enabled for the specified channel
# Protocol arguments:
#  1. Serial address
#  2. The combination channel number; 1 or 2
################################################################################
get_combination_channel {
    out  $start_comm "EPC\$2?";
    in   $ack_resp   $dis_enable;
}

################################################################################
# Enable or disable the combination channel for the specified channel
# Protocol arguments:
#  1. Serial address
#  2. The combination channel number; 1 or 2
################################################################################
set_combination_channel {
    out  $start_comm "EPC\$2!" $dis_enable;
    in   $ack_resp             $dis_enable;
}

################################################################################
# Check if Pascal is forced as the pressure unit
# Protocol arguments:
#  1. Serial address
################################################################################
get_force_pascal {
    out  $start_comm "IU?";
    in   $ack_resp   $on_off;
}

################################################################################
# Forces or not the use of Pascal as the pressure unit
# Protocol arguments:
#  1. Serial address
################################################################################
set_force_pascal {
    out  $start_comm "IU!" $on_off;
    in   $ack_resp         $on_off;
}

################################################################################
# Retrieves the error mode of a NAK response
# Protocol arguments:
#  1. Serial address
################################################################################
error_modes = "%{TXT|CODE}";
get_error_mode {
    out  $start_comm "SEM?";
    in   $ack_resp   $error_modes;
}

################################################################################
# Sets the error mode of a NAK response
# Protocol arguments:
#  1. Serial address
################################################################################
set_error_mode {
    out  $start_comm "SEM!" $error_modes;
    in   $ack_resp          $error_modes;
}

################################################################################
# Retrieves the screen saver timeout value
# Protocol arguments:
#  1. Serial address
################################################################################
get_ss_timeout {
    out  $start_comm "SST?";
#    in   $ack_resp   "%#/OFF/0/" "%d";
    in   $ack_resp   "%?d";
}

################################################################################
# Sets the screen saver timeout value
# Protocol arguments:
#  1. Serial address
################################################################################
set_ss_timeout {
    out  $start_comm "SST!%d";
    in   $ack_resp   "%?d";
}

################################################################################
################################################################################
##
## Relays
##
################################################################################
################################################################################

################################################################################
# Retrieves the setpoint of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
get_relay_setpoint {
    out $start_comm "SP\$2?";
    in  $ack_resp   "%f";
}

################################################################################
# Sets the setpoint of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
set_relay_setpoint {
    out $start_comm "SP\$2!%f";
    in  $ack_resp   "%f";
}

################################################################################
# Retrieves the setpoint hysteresis of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
get_relay_setpoint_hyst {
    out $start_comm "SH\$2?";
    in  $ack_resp   "%f";
}

################################################################################
# Sets the the setpoint hysteresis of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
set_relay_setpoint_hyst {
    out $start_comm "SH\$2!%f";
    in  $ack_resp   "%f";
}

################################################################################
# Retrieves the setpoint direction of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
relay_directions = "%{ABOVE|BELOW}";
get_relay_setpoint_direction {
    out $start_comm "SD\$2?";
    in  $ack_resp   $relay_directions;
}

################################################################################
# Sets the setpoint direction of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
set_relay_setpoint_direction {
    out $start_comm "SD\$2!" $relay_directions;
    in  $ack_resp            $relay_directions;
}

################################################################################
# Retrieves the status of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
relay_statuses = "%{CLEAR|SET|ENABLE}";
get_relay_status {
    out $start_comm "EN\$2?";
    in  $ack_resp   $relay_statuses;
}

################################################################################
# Sets the status of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
set_relay_status {
    out $start_comm "EN\$2!" $relay_statuses;
    in  $ack_resp            $relay_statuses;
}

################################################################################
# Retrieves the status of all relays
# Protocol arguments:
#  1. Serial address
################################################################################
get_relay_statuses {
    out $start_comm "ENA?";
    in  $ack_resp   "%(A)1d"
                    "%(B)1d"
                    "%(C)1d"
                    "%(D)1d"
                    "%(E)1d"
                    "%(F)1d"
                    "%(G)1d"
                    "%(H)1d"
                    "%(I)1d"
                    "%(J)1d"
                    "%(K)1d"
                    "%(L)1d";
}

################################################################################
# Retrieves the setpoint status of all relays
# Protocol arguments:
#  1. Serial address
################################################################################
get_relay_setpoint_statuses {
    out $start_comm "SSA?";
    in  $ack_resp   "%(A)1d"
                    "%(B)1d"
                    "%(C)1d"
                    "%(D)1d"
                    "%(E)1d"
                    "%(F)1d"
                    "%(G)1d"
                    "%(H)1d"
                    "%(I)1d"
                    "%(J)1d"
                    "%(K)1d"
                    "%(L)1d";
}

################################################################################
# Retrieves the setpoint status of the specified relay
# Protocol arguments:
#  1. Serial address
#  2. Relay number 1..12
################################################################################
get_relay_setpoint_status {
    out $start_comm "SS\$2?";
    in  $ack_resp   "%{SET|CLEAR}";
}

################################################################################
################################################################################
##
## Pressure reading
##
################################################################################
################################################################################

################################################################################
# Retrieves the pressure reading on all the 6 channels
# Requires an scalcout record
# Protocol arguments:
#  1. Serial address
################################################################################
get_pressures {
    out $start_comm "PRZ?";
    in  $ack_resp   "%(AA)s "
                    "%(BB)s "
                    "%(CC)s "
                    "%(DD)s "
                    "%(EE)s "
                    "%(FF)s";
}
