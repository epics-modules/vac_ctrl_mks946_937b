################################################################################
# Database for MKS946 Vacuum Mass Flow & Gauge Controller
# Database for MKS937B Gauge Controller
# Pressure readings on the different channels
# CHANNEL=A1,A2,B1,B2,C1,C2
# N=A to F
################################################################################

#
# Read the pressure value as a string from scalcout
# Convert pressure string to double
# Check if pressure string is LO<E-xx
# Read pressure value as converted by iChkURng
# Update Pressure
# Update Pressure Status
#
record(fanout, "${P}${R}${CHANNEL}:iPrsProc") {
    field(LNK1, "${P}${R}${CHANNEL}:PrsR-STR")
    field(LNK2, "${P}${R}${CHANNEL}:iPrsR")
    field(LNK3, "${P}${R}${CHANNEL}:iChkURng")
    field(LNK4, "${P}${R}${CHANNEL}:iPrs2R")
    field(LNK5, "${P}${R}${CHANNEL}:PrsR")
    field(LNK6, "${P}${R}${CHANNEL}:iChkPrs")
}


# Read the pressure value as a string from scalcout
record("*", "${P}${R}${CHANNEL}:PrsR-STR") {
    field(INP,  "${P}${R}iPressuresR.${N}${N} MSS")
}


# Convert pressure string to double
record(ai, "${P}${R}${CHANNEL}:iPrsR") {
    field(DESC, "Pressure reading of channel ${CHANNEL}")
    field(INP,  "${P}${R}${CHANNEL}:PrsR-STR MSS")
}


# Convert pressure string to double (replacing 'LO<E-xx' with '1E-xx', 'ATM' with '5.2E+2')
# and return if string contains 'LO<E-xx' aka Under-Range
# DBL(x) is used because:
#  Explicit conversion to a number (by DBL, INT, or NINT) is more aggressive than implicit conversion, and skips leading non-numeric characters.
# and there are responses like >1.1E+4
# Make sure not to overwrite the initial conversion (ie, 'A') because iChkPrs needs to check its value
record(scalcout, "${P}${R}${CHANNEL}:iChkURng") {
    field(INAA, "${P}${R}${CHANNEL}:PrsR-STR MSS")
    field(INPB, "${P}${R}${CHANNEL}:InvPrsS")
    field(CALC, "A:=DBL(AA{'LO<','1'}{'ATM','5.2E+2'}); B:=(A==0 ? B : A); INT(AA[0,2] == 'LO<')")
}


# Read pressure value as converted by scalcout
record(ai, "${P}${R}${CHANNEL}:iPrs2R") {
    field(DESC, "Pressure reading of channel ${CHANNEL}")
    field(INP,  "${P}${R}${CHANNEL}:iChkURng.B MSS")
}


# The pressure value as converted by scalcout
record("*", "${P}${R}${CHANNEL}:PrsR") {
    field(INP,  "${P}${R}${CHANNEL}:iPrs2R MSS")
}


#
# 1: IF pressure string is a number
# 2: IF pressure string is LO<E-xx
# 3: IF pressure string contains a number
# 4: otherwise
#
record(calcout, "${P}${R}${CHANNEL}:iChkPrs") {
    field(INPA, "${P}${R}${CHANNEL}:iPrsR.STAT")
    field(INPB, "${P}${R}${CHANNEL}:iPrsR.SEVR")
    field(INPC, "${P}${R}${CHANNEL}:iChkURng")
    field(INPD, "${P}${R}${CHANNEL}:iChkURng.A MSS")
    field(CALC, "((A || B) == 0) ? 1 : (C ? 2 : (D ? 3 : 4))")
    field(OUT,  "${P}${R}${CHANNEL}:iPrsStatVR.SELN MSS PP")
}


# switch case
record(seq, "${P}${R}${CHANNEL}:iPrsStatVR") {
    field(DISP, "1")

#   set status to VALID_PRESSURE
    field(DO1,  "0")
    field(LNK1, "${P}${R}${CHANNEL}:iPrsStat MSS PP")

#   set status to UNDER-RANGE
    field(DO2,  "1")
    field(LNK2, "${P}${R}${CHANNEL}:iPrsStat MSS PP")

#   set status to OVER-RANGE (aka ATM)
    field(DO3,  "2")
    field(LNK3, "${P}${R}${CHANNEL}:iPrsStat MSS PP")

#   try to parse the pressure string as ENUM
    field(DO4,  "1")
    field(LNK4, "${P}${R}${CHANNEL}:iPrsStatInit.PROC")

    field(SELM, "Specified")
}


# Initialize pressure status to <<INVALID>>
# Forward process parsing pressure string as ENUM
record(longout, "${P}${R}${CHANNEL}:iPrsStatInit") {
    field(DESC, "Set iPrsStat to INVALID")
    field(DISP, "1")
    field(VAL,  "15")
    field(OUT,  "${P}${R}${CHANNEL}:iPrsStat PP")
    field(FLNK, "${P}${R}${CHANNEL}:iPrsParse")
}


# Predefined pressure value of invalid status
record(ao, "${P}${R}${CHANNEL}:InvPrsS") {
    field(VAL,  "1000")
    field(PINI, "YES")
}


# Write pressure string to pressure status ENUM
record(stringout, "${P}${R}${CHANNEL}:iPrsParse") {
    field(DESC, "Parse pressure string")
    field(DOL,  "${P}${R}${CHANNEL}:PrsR-STR MSS")
    field(OMSL, "closed_loop")
    field(OUT,  "${P}${R}${CHANNEL}:iPrsStat MSS PP")
}


# The status of channel ${CHANNEL}
#  (as converted from pressure string)
record(mbbi, "${P}${R}${CHANNEL}:iPrsStat") {
    field(DESC, "Status of channel ${CHANNEL}")

    field(ZRST, "VALID_PRESSURE")

    field(ONST, "UNDER_RANGE")

    field(TWST, "ATM")

    field(THST, "OFF")

    field(FRST, "RP_OFF")

    field(FVST, "WAIT")

    field(SXST, "LowEmis")

    field(SVST, "CTRL_OFF")

    field(EIST, "PROT_OFF")

    field(NIST, "MISCONN")

    field(TEST, "NO_GAUGE")

    field(ELST, "NOGAUGE")

    field(TVST, "o12o")
    field(TTST, "o13o")

    field(FTVL, "14")
    field(FTST, "")

    field(FFST, "<<INVALID>>")
    field(FFSV, "MAJOR")

    field(UNSV, "MAJOR")

    field(VAL,  "15")
    field(PINI, "YES")

    field(FLNK, "${P}${R}${CHANNEL}:iPrsStatS")
}


record(mbbo, "${P}${R}${CHANNEL}:iPrsStatS") {
    field(DOL,  "${P}${R}${CHANNEL}:iPrsStat MSS")
    field(OMSL, "closed_loop")
    field(OUT,  "${P}${R}${CHANNEL}:PrsStatR MSS PP")
    field(DTYP, "Raw Soft Channel")

    # ON:
    #  -  0 -- VALID_PRESSURE
    field(ZRVL, "1")

    # OFF:
    #  -  3 -- OFF
    #  -  4 -- RP_OFF
    #  -  7 -- CTRL_OFF
    #  -  8 -- PROT_OFF
    field(THVL, "2")
    field(FRVL, "2")
    field(SVVL, "2")
    field(EIVL, "2")

    # ERROR:
    #  -  5 -- WAIT
    #  -  6 -- LowEmis
    #  -  9 -- MISCONN
    #  - 10 -- NO_GAUGE
    #  - 11 -- NOGAUGE
    #  - 15 -- INVALID
    field(FVVL, "3")
    field(SXVL, "3")
    field(NIVL, "3")
    field(TEVL, "3")
    field(ELVL, "3")
    field(FTVL, "3")
    field(FFVL, "3")

    #
    # FIXME: over-range is not correct yet.
    # ATM is clearly overrange but the documentation is not clear:
    #  it does not mention this but I've seen responses like >1.1E+4. I assume it is overrange
    #

    # OVER-RANGE:
    #  -  2 -- ATM
    field(TWVL, "4")

    # UNDER-RANGE:
    field(ONVL, "5")
}
