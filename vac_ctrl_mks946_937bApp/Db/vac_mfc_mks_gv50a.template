################################################################################
# Database for mass flow controller for
# MKS946 Vacuum Mass Flow & Gauge Controller
################################################################################

record("*", "${P}${R}iEGUChRec") {
    # Disable EGU change detection
    field(DOL1, "")
}


alias("${P}${R}PrsR",        "${P}${R}FlwR")
alias("${P}${R}PrsR-STR",    "${P}${R}FlwR-STR")

record("*", "${P}${R}iCheckType") {
    field(CALC, "(A&&AA=='FC')?'1':'0'")
}


#FlwR is an alias to PrsR and PrsR is set to mbar, let's fix that
record(stringout, "${P}${R}iFlwEGU") {
    field(DESC, "Fix flow unit")
    field(PINI, "YES")
    field(VAL,  "sccm")
    field(OUT,  "${P}${R}FlwR.EGU")
}


record(ai, "${P}${R}ScaleF-RB") {
    field(DESC, "Get scale factor")
    field(DTYP, "stream")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_scale_factor(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}ScaleFS") {
    field(DESC, "Set scale factor")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_scale_factor(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(DRVL, "0.1")
    field(LOPR, "0.1")
    field(DRVH, "50")
    field(HOPR, "50")

    field(FLNK, "${P}${R}ScaleF-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ai, "${P}${R}NomRng-RB") {
    field(DESC, "Get full scale nominal range")
    field(DTYP, "stream")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(EGU,  "sccm")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}NomRngS") {
    field(DESC, "Set full scale nominal range")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_range(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "sccm")
    field(DRVH, "1000000")
    field(DRVL, "0.1")
    field(HOPR, "1000000")
    field(LOPR, "0.1")

    field(FLNK, "${P}${R}NomRng-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bo, "${P}${R}ZeroS") {
    field(DESC, "Zero the MFC")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_zero(${SERADDR},${CHANNEL_N},${P}${R}Zero-RB) ${ASYNPORT}")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bi, "${P}${R}Zero-RB") {
    field(DESC, "Result of Zero operation")
    field(ZNAM, "Failed")
    field(ONAM, "OK")
}


record(ai, "${P}${R}FlwSP-RB") {
    field(DESC, "Get flow setpoint")
    field(DTYP, "stream")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_flow_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(EGU,  "sccm")
    field(PREC, "2")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(ao, "${P}${R}FlwSPS") {
    field(DESC, "Set flow setpoint")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_flow_setpoint(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(EGU,  "sccm")
    field(PREC, "2")
    field(HOPR, "1000")
    field(LOPR, "0")

    field(FLNK, "${P}${R}FlwSP-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(bi, "${P}${R}iCntTimElapsed") {
    field(DESC, "If time should be tracked")
    field(PINI, "YES")
    field(VAL,  "0")
}


record(longout, "${P}${R}FlwInRngTimeoS") {
    field(DESC, "Time to reach 10% of FlwSP-RB")
    field(EGU,  "sec")
    field(PINI, "YES")
    field(DRVL, "1")
    field(DRVH, "100")
    field(VAL,  "10")
    field(OUT,  "${P}${R}FlwSPTimElapsedR.HIGH")
}


record(calcout, "${P}${R}iFlwSPTimElapsedR") {
    field(DESC, "Calc time to reach 10% of FlwSP-RB")
    field(INPA, "${P}${R}iFlwSPTimElapsedR")
    field(CALC, "A < 120 ? A + 1 : A")
    field(SCAN, "1 second")
    field(OUT,  "${P}${R}FlwSPTimElapsedR PP")
    field(DISV, "0")
    field(SDIS, "${P}${R}iCntTimElapsed")
}


record(longin, "${P}${R}FlwSPTimElapsedR") {
    field(DESC, "Time it took to reach 10% of FlwSP-RB")
    field(EGU,  "sec")
    field(HSV,  "MINOR")
    field(HIHI, "120")
    field(HHSV, "MAJOR")
}


record(bi, "${P}${R}FlwInRngR") {
    field(DESC, "Flow rate is within range of FlwSP-RB")
    field(ONAM, "In range")
    field(ZNAM, "Out of range")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR CP")
}


#Set by check_flow_rate.st
record(bi, "${P}${R}FlwRngTimeoR") {
    field(DESC, "Flow rate range of FlwSP-RB timeout")
    field(ONAM, "Timeout")
    field(OSV,  "MAJOR")
}


record(calcout, "${P}${R}iChkFlwInRng") {
    field(DESC, "Check if flow rate is within range")
    field(INPA, "${P}${R}FlwSP-RB CP")
    field(INPB, "${P}${R}FlwR CP")
    field(CALC, "A ? (abs(1-A/B) <= 0.1) : (B <= 0.1)")
    field(OUT,  "${P}${R}FlwInRngR PP")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


#
# WARNING
# Whenever you change / reorder values here go and update check_flow_rate.st
# The integer value of Sepoint needs to be in sync between these two and check_flow_rate.st
#
record(mbbi, "${P}${R}Mode-RB") {
    field(DESC, "Get control enabled status")
    field(DTYP, "stream")
    field(INP,  "@vac_mfc_mks_gv50a.proto get_mode(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "Closed")

    field(ONVL, "1")
    field(ONST, "Setpoint")

    field(TWVL, "2")
    field(TWST, "Open")

    field(THVL, "3")
    field(THST, "PID Ctrl")

    field(FRVL, "4")
    field(FRST, "Ratio A")

    field(FVVL, "5")
    field(FVST, "Ratio M")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


record(mbbo, "${P}${R}ModeS") {
    field(DESC, "Set control enabled status")
    field(DTYP, "stream")
    field(OUT,  "@vac_mfc_mks_gv50a.proto set_mode(${SERADDR},${CHANNEL_N}) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "Close")

    field(ONVL, "1")
    field(ONST, "Setpoint")

    field(TWVL, "2")
    field(TWST, "Open")

    field(FLNK, "${P}${R}Mode-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}ValidR")
}


#
# FlwStatR logic
#
record(calc, "${P}${R}iFlwStat-FO") {
    field(DESC, "PV to trigger UI status update")
    field(INPA, "${P}${R}PrsStatR CP")
    field(INPB, "${P}${R}Mode-RB CP")
    field(INPC, "${P}${R}FlwRngTimeoR CP")
    field(INPD, "${P}${R}FlwInRngR CP")
    field(CALC, "A")
    field(FLNK, "${P}${R}iFlwStatSeq")
}


record(seq, "${P}${R}iFlwStatSeq") {
    field(SELM, "Specified")
    field(SELL, "${P}${R}PrsStatR MSS")
    field(OFFS, "1")

# ON
    field(DOL2, "1")
    field(LNK2, "${P}${R}iModeSeq MSS PP")

# ERROR
    field(DOL4, "3")
    field(LNK4, "${P}${R}FlwStatR MSS PP")

# INVALID
    field(DOL1, "0")
    field(LNK1, "${P}${R}FlwStatR MSS PP")
# OFF
    field(DOL3, "0")
    field(LNK3, "${P}${R}FlwStatR MSS PP")
# OVER-RANGE
    field(DOL5, "0")
    field(LNK5, "${P}${R}FlwStatR MSS PP")
# UNDER-RANGE
    field(DOL6, "0")
    field(LNK6, "${P}${R}FlwStatR MSS PP")
}


record(seq, "${P}${R}iModeSeq") {
    field(DISP, "1")
    field(SELM, "Specified")
    field(SELL, "${P}${R}Mode-RB MSS")
    field(OFFS, "1")

# Closed
    field(DOL1, "2")
    field(LNK1, "${P}${R}FlwStatR MSS PP")

# Setpoint
    field(DOL2, "1")
    field(LNK2, "${P}${R}iChkFlwRngStat MSS PP")

# Open
    field(DOL3, "1")
    field(LNK3, "${P}${R}FlwStatR MSS PP")

# PID Ctrl
    field(DOL4, "0")
    field(LNK4, "${P}${R}FlwStatR MSS PP")
# Ratio A
    field(DOL5, "0")
    field(LNK5, "${P}${R}FlwStatR MSS PP")
# Ratio M
    field(DOL6, "0")
    field(LNK6, "${P}${R}FlwStatR MSS PP")
}


record(calcout, "${P}${R}iChkFlwRngStat") {
    field(DISP, "1")
# Do not MSS this one. It can be UDF until a setpoint change
    field(INPA, "${P}${R}FlwRngTimeoR")
    field(INPB, "${P}${R}FlwInRngR MSS")
    field(CALC, "A ? 3 : (B ? 1 : 0)")
    field(OUT,  "${P}${R}FlwStatR MSS PP")
}


record(mbbi, "${P}${R}FlwStatR") {
    field(ZRVL, "0")
    field(ZRST, "Invalid")

    field(ONVL, "1")
    field(ONST, "Open")

    field(TWVL, "2")
    field(TWST, "Closed")

    field(THVL, "3")
    field(THST, "Error")
    field(THSV, "MAJOR")
}
