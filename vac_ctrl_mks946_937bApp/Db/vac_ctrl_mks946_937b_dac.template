################################################################################
# Database for MKS946 Vacuum Mass Flow & Gauge Controller
# Database for MKS937B Gauge Controller
# DAC parameters per channel
# n=0 to 6, and is the channel number, 0 is the combination output
# CHANNEL is a meaningful name instead of 0..6
################################################################################

record(mbbi, "${P}${R}${CHANNEL}:DACType-RB") {
    field(DESC, "Query the type of DAC")
    field(DTYP, "stream")
    field(INP,  "@vac_ctrl_mks946_937b_channel.proto get_DAC_output(${SERADDR},${n}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(ZRVL, "0")
    field(ZRST, "Linear")

    field(ONVL, "1")
    field(ONST, "Logarithmic")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(mbbo, "${P}${R}${CHANNEL}:DACTypeS") {
    field(DESC, "Set the type of DAC")
    field(DTYP, "stream")
    field(OUT,  "@vac_ctrl_mks946_937b_channel.proto set_DAC_output(${SERADDR},${n}) ${ASYNPORT}")

    field(ZRVL, "0")
    field(ZRST, "Linear")

    field(ONVL, "1")
    field(ONST, "Logarithmic")

    field(FLNK, "${P}${R}${CHANNEL}:DACType-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(ai, "${P}${R}${CHANNEL}:DACSlope-RB") {
    field(DESC, "Query the DAC slope parameter")
    field(DTYP, "stream")
    field(INP,  "@vac_ctrl_mks946_937b_channel.proto get_DAC_slope(${SERADDR},${n}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(ao, "${P}${R}${CHANNEL}:DACSlopeS") {
    field(DESC, "Set the DAC slope parameter")
    field(DTYP, "stream")
    field(OUT,  "@vac_ctrl_mks946_937b_channel.proto set_DAC_slope(${SERADDR},${n}) ${ASYNPORT}")

#FIXME: these limits are only valid if DAC is set to LOG
    field(DRVL, "0.5")
    field(DRVH, "5")
#    field(DRVL, "1e-8")
#    field(DRVH, "1e-4")
    field(FLNK, "${P}${R}${CHANNEL}:DACSlope-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(ai, "${P}${R}${CHANNEL}:DACOffset-RB") {
    field(DESC, "Query the DAC offset parameter")
    field(DTYP, "stream")
    field(INP,  "@vac_ctrl_mks946_937b_channel.proto get_DAC_offset(${SERADDR},${n}) ${ASYNPORT}")
    field(SCAN, "Event")
    field(EVNT, "${P}-RB")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}


record(ao, "${P}${R}${CHANNEL}:DACOffsetS") {
    field(DESC, "Set the DAC offset parameter")
    field(DTYP, "stream")
    field(OUT,  "@vac_ctrl_mks946_937b_channel.proto set_DAC_offset(${SERADDR},${n}) ${ASYNPORT}")

#FIXME: these limits are only valid if DAC is set to LOG
    field(DRVL, "-20")
    field(DRVH, "20")
#    field(DRVL, "0")
#    field(DRVH, "0")
    field(FLNK, "${P}${R}${CHANNEL}:DACOffset-RB.PROC CA")

    field(DISV, "0")
    field(DISS, "INVALID")
    field(SDIS, "${P}${R}CommsOK")
}
