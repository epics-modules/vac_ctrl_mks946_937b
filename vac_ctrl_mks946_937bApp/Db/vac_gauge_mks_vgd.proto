################################################################################
# Protocol file for capacitance manometer gauge for
# MKS946 Vacuum Mass Flow & Gauge Controller or
# MKS937B Gauge Controller
################################################################################

Terminator  = ";FF";

ReadTimeout   = 5000;
WriteTimeout  = 5000;
PollPeriod    = 100;
ReplyTimeout  = 5000;
LockTimeout   = 30000;

ExtraInput    = Ignore;

start_comm    = "@254";
ack_resp      = "@%*dACK";
nak_resp      = "@%*dNAK";
start_comm    = "@%(\$1)03d";
ack_resp      = "@%(\$1)=03dACK";
nak_resp      = "@%(\$1)=03dNAK";

channel       = "%(\$2)d";
on_off        = "%{OFF|ON}";

reconnect {
    disconnect;
    wait 1000;
    connect 1000;
}
@replytimeout {
    reconnect;
}

################################################################################
################################################################################
#
# Capacitance Manometer
#
################################################################################
################################################################################

################################################################################
# Get full scale pressure measurement range
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
get_pressure_range {
    out $start_comm "RNG" $channel "?";
    in  $ack_resp   "%f";

    @mismatch {
        in $nak_resp "155";
    }
}

################################################################################
# Set full scale pressure measurement range
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_pressure_range {
    out $start_comm "RNG" $channel "!%f";
    in  $ack_resp   "%f";
}

################################################################################
# Get the capacitance manometer type
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
types = "%{ABS|DIFF}";
get_type {
    out $start_comm "CMT" $channel "?";
    in  $ack_resp   $types;

    @mismatch {
        in $nak_resp "155";
    }
}

################################################################################
# Set the capacitance manometer type
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_type {
    out $start_comm "CMT" $channel "!" $types;
    in  $ack_resp                      $types;
}

################################################################################
# Get full scale voltage output range
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
fs_voltages = "%{1|5|10|1U|5U|10U|1B|5B}";
get_voltage_range {
    out $start_comm "BVR" $channel "?";
    in  $ack_resp   $fs_voltages;

    @mismatch {
        in $nak_resp "155";
    }
}

################################################################################
# Set full scale voltage output range
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_voltage_range {
    out $start_comm "BVR" $channel "!" $fs_voltages;
    in  $ack_resp                      $fs_voltages;
}

################################################################################
# Zero the capacitance manometer
# Protocol arguments:
#  1. Serial address
#  2. Channel
#  3. Result PV
################################################################################
set_zero {
    out $start_comm "VAC" $channel "!";
    in  $ack_resp   "%(\$3){NAK|OK}";
}

################################################################################
# Get autozero control channel
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
az_channels = "%{NA|A1|A2|B1|B2|C1|C2}";
get_autozero_channel {
    out $start_comm "AZ" $channel "?";
    in  $ack_resp   $az_channels;

    @mismatch {
        in $nak_resp "155";
    }
}

################################################################################
# Set autozero control channel
# Protocol arguments:
#  1. Serial address
#  2. Channel
################################################################################
set_autozero_channel {
    out $start_comm "AZ" $channel "!" $az_channels;
    in  $ack_resp                     $az_channels;
}
